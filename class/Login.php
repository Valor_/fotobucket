<?php 
class Login extends Database{

    public function __construct(){
        $conn = Database::constructConnection();
    }

    // Logs in the user
    public function loginUser() {
        // Set variables
        $username = $_POST["username"];
        $pwd = $_POST["pwd"];
        // Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($username, $pwd); //Sanatizes and validates the Username addres 

        // Check if sanVal is valid
        if($sanVal !== false){
            // Check if the info is correct....
            $checkInfo = $this->checkInfo($sanVal["Username"], $sanVal["Password"]); //Gets back an assosicative array containing the values        
            if($checkInfo && password_verify($sanVal["Password"], $checkInfo["Password"]) && $checkInfo["Username"] == $sanVal["Username"]){
                // If it is correct log in and set the user_id
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $checkInfo["UserId"];
        
                header("Location: albums"); // Redirect browser
                exit();

            } else{
                // Error handling
                $this->displayMessage("Incorrect username or password", "red");
            }
        } else{
            //Error handling
            $this->displayMessage("Something went wrong please try again!", "red");
        }
    }
    
    // Log out the user
    public function logoutUser(){
        session_start();
        unset($_SESSION);
        session_destroy();
        session_write_close();

        // Refer back to login page
        header('Location: login');
        die;
    }

    // Note: Older deprecated version of this function, but still used
    // Validates and sanatizes the postvalues
    private function validateSanatize($username, $password) {
        $dataArray = array(
            $username,
            $password
        );
        $res = array();
        
        // Loop through al data and Validate / Sanatize it
        for ($i = 0; $i < count($dataArray); $i++) {
            $str = addslashes($dataArray[$i]);
            $str = preg_replace("/<script>|<\/script>/i", "", $str);
            $str = preg_replace("/<|>/i", "", $str);
            $str = strip_tags($str);
            
            array_push($res, $str);
        }
            // Set the array and return it
            $finalRes = array(
                "Username" => $res[0],
                "Password" => $res[1]
            );
            return $finalRes;
    }

    // Check if the info matches up with what is in the database
    private function checkInfo($username, $password) {
        $check = $this->dbh->prepare("SELECT `id`, `name`, `email`, `password` FROM `users` WHERE `name` = ? LIMIT 1");
        $check->execute(array(
            $username, 
        ));
        // Get the result of the check
        $checkResult = $check->fetch();

        // If its valid return all values in array
        if($checkResult){
            $finalCheckRes = array(
                "UserId" => $checkResult[0],
                "Username" => $checkResult[1],
                "Email" => $checkResult[2],
                "Password" => $checkResult[3],
            );
            return $finalCheckRes;
        }
        else{
            return false;
        }
        
    }

        private function displayMessage($error, $color) {
            echo "<div class='col-sm-12' style='color: $color;'><p>$error</p></div>";
    }
}
?>