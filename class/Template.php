<?php
class Template extends Config {

    //  Set the template
    // $page: The page
    // $dir: The directory
    public function setTemplate($page, $dir)
    {
     $dir = str_replace('\\', '/', $dir);

         // Gets end bit of url
        $path = parse_url($page, PHP_URL_PATH);
        $pathFragments = explode('/', $path);
        $end_bit = end($pathFragments);


        // Pages u dont have to be logged in on
        $exceptions = [
            'login',
            'register',
        ];

        // Exceptions to purely not loading the header and footer
        $hf_exceptions = [
            'album-view',
        ];

        // Basically checks if its the root of the document if it is sets page to login
        if(empty($end_bit)){
            $end_bit = 'login';
        }

            // Create the path
            $path = $this->project_base . "/" . "templates/" . "$end_bit/" . $end_bit . ".php";
            if(file_exists($path)){
                
            // Start session
            session_start();

            // Security token
            if (empty($_SESSION['csrf_token'])) {
                $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
            }
            // Check if current page is an exception to loading the standardized header and footer
            $pre_login = false;
            $hf_exception = false;
            for($x = 0; $x <= count($exceptions ) - 1; $x++){
                if($end_bit == $exceptions[$x]){
                    $pre_login = true;
                }
            }

            for($i = 0; $i < count($hf_exceptions); $i++) {
                if($end_bit == $hf_exceptions[$i]) {
                    $hf_exception = true;
                }
            }

                // Trying to view a page while not loggedin thats not on the exception list
                if(!isset($_SESSION['loggedin']) && $pre_login == false) {
                    echo "<h2 style='text-align: center;'>403 - LOGIN TO VIEW THESE PAGES</h2>";
                    die();
                }

                // If You`re loggedin OR you`re not on a page not requiring login
                if(isset($_SESSION['loggedin']) || $pre_login == true){ 
                    require_once 'templates/_parts/head.php';

                    // Check if there is an exception to loading the standard header and footer
                    if(isset($_SESSION['loggedin']) && $pre_login == false && $hf_exception == false){
                        require_once 'templates/_parts/header.php';
                        require_once $path;
                        require_once 'templates/_parts/footer.php';
                    }
                    else { // If there is dont load them
                        require_once $path;
                    }
                    require_once 'templates/_parts/foot.php';
                    exit;
                    }

                }
            else{
                echo "404 - The page you`re looking for doesn`t exist!";
                die();
            }
        }
    }
?>