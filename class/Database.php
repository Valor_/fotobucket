
<?php 
class Database 
{
    // Values..
     const DB_HOST = '127.0.0.1';
     const DB_USER = 'root';
     const DB_PASS = '';
     const DB_NAME = 'foto_bucket';

    // Construct a connection based on the values
    public $dbh;
    protected function constructConnection()
    {
        try
        {
            $this->dbh = new PDO("mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_NAME, self::DB_USER, self::DB_PASS);
        
            $this->dbh
                ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            // Error handling
            // echo $e->getMessage();
        }
    }

    public function __construct()
    {
        $conn = $this->constructConnection();
    }
    // NOTE: Some function have been omitted because they are never used in the project
}   
?>