<?php
class Register extends Database
{
    
    public function __construct()
    {
        $conn = $this->constructConnection();
    }
    
    // Registers a user
    public function registerUser() {
        // Set variables
        $email    = $_POST["email"];
        $username = $_POST["username"];
        $password = $_POST["pwd"];
        
        //Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($email, $username, $password);
        if ($sanVal !== false) {
            // Check if user already exists
            $exists = $this->existsCheck($username, $email);

            // If it doesnt add it
            if ($exists == false) {
                $sanVal["Password"] = password_hash($sanVal["Password"], PASSWORD_DEFAULT);
                $lowerEmail = strtolower($sanVal["Email"]);
                
                // Prepare bind and execute the query
                $query = $this->dbh->prepare("INSERT INTO users (`name`, `email`, `password`) 
                    VALUES (:user_name, :e_mail, :pass)");

                $query->bindParam(':user_name', $sanVal["Username"]);
                $query->bindParam(':e_mail', $lowerEmail);
                $query->bindParam(':pass', $sanVal["Password"]);
                $query->execute();

                 // Redirect browser
                header("Location: login");
                
            } else {
                // Error handling
                $this->displayMessage("Email or username already in use!", "red");
            }
        } else {
            // Error handling
            $this->displayMessage("Please enter a valid email adress", "red");
        }
    }
    
    // Check if user already exists
    // $username: The username  of the user
    // $email: Email of the user
    private function existsCheck($username, $email) {
        // Prepare and fetch the resul of the username check
        $existsUsername = $this->dbh->prepare("SELECT `name`  FROM users WHERE `name` = ?");
        $existsUsername->execute(array($username));
        $userRes = $existsUsername->fetch();

        // Prepare and fetch the resul of the email check
        $existsEmail = $this->dbh->prepare("SELECT `email`  FROM users WHERE `email` = ?");
        $existsEmail->execute(array(
            $email
        ));

        $emailRes = $existsEmail->fetch();
        // If even one is invalid return false if not return true
        if ($userRes || $emailRes) {
            return true;
        } else {
            return false;
        }
    }

    // Note: Older version of this function but still used
    // Validate and sanatize the postvalues
    private function validateSanatize($email, $username, $password) {
        $dataArray = array(
            $email,
            $username,
            $password
        );
        $res = array();

        // Loop trough all data validate and return it
        for ($i = 0; $i < count($dataArray); $i++) {
            $str = addslashes($dataArray[$i]);
            $str = preg_replace("/<script>|<\/script>/i", "", $str);
            $str = preg_replace("/<|>/i", "", $str);
            $str = strip_tags($str);
            
            array_push($res, $str);
        }
        if (filter_var($res[0], FILTER_VALIDATE_EMAIL)) {
            $finalRes = array(
                "Email" => $res[0],
                "Username" => $res[1],
                "Password" => $res[2]
            );

            // Return it
            return $finalRes;
        } else {
            return false;
        }
    }

    // Display a message
    private function displayMessage($error, $color) {
        echo "<div class='col-sm-12' style='color: $color;'><p>$error</p></div>";
    }
}
?>