<?php
class Files extends Database
{

    public function __construct() {
        $conn = Database::constructConnection();
    }


    // NOTE: Standard function has been modified for this project
    // Saves filesto the file strucutre
    // $files: The files
    // $upload_dir: the upload directory
    public function save_files($files, $upload_dir = false) {
        $config = new Config;
        $fileArray = [];
        $returnPath = false;

        // Check if $upload_dir has a custom directory 
        if($upload_dir == false) {
            // Either select to return the filepath or all files in an array
            $returnPath = true;
            $upload_dir = "user-files/" . $_SESSION['user_id'] . "/" . round(microtime(true)).mt_rand();
        }
        $success = false;
        $total_size = 0;

        try {
            if(isset($files['userfiles'])) {
                // Loop thru all files to get the totalsize
                foreach($files["userfiles"]["size"] as $key) {
                    $total_size = $total_size + $key;
                }
                // Check if the total size doesnt exceed the limit
                if($total_size > 52428800) {
                    echo "<div class='col-sm-12'><p>Files are too big!</p></div>";
                }
                else {
                    // Check if the upload_dir exists if not create it
                    if ( !file_exists( $upload_dir ) && !is_dir( $upload_dir ) ) {
                        mkdir( $upload_dir, 0700, true );       
                    }
                    // Loop thru all files
                    foreach ($files["userfiles"]["tmp_name"] as $key => $error) {
                            $tmp_name = $files["userfiles"]["tmp_name"][$key];
                            // Basename() may prevent filesystem traversal attacks
                            $name = basename($files["userfiles"]["name"][$key]);    
                            // Get extension,  filename and move the fiile to its directory
                            $ext = strtolower(substr($name, strripos($name, '.')+1));
                            $filename = round(microtime(true)).mt_rand().'.'.$ext;
                            move_uploaded_file($tmp_name, "$upload_dir/$filename");

                            // Push to filearray
                            array_push($fileArray, $filename);
                    }
                }
            }
        } catch (RuntimeException $e)  {
            echo $e->getMessage();
        }
        // Either we return the path of the directory or array of files based on $returnPath
        if($returnPath == true) {
            return $upload_dir;
        } else {
            return $fileArray;
        }
    }
    
    // FUNCTION: Deletes all files in a given path recursively...
    // $file_path: Path to the directory containing the files.
    public function delete_files_folder($file_path) {
        if(is_dir($file_path)) {
            // Unlink all the files in the directory this has to be done before the directory can be removed
            array_map('unlink', glob("$file_path/*.*"));

            // Remove the directory
            rmdir($file_path);
        } else {
            return false;
        }
    }

    // FUNCTION: Removes all given files in an array
    // $items: All items to be removed in a simple array
    // $file_path: path to where the files are
    public function delete_files($items, $file_path) {
        if(is_dir($file_path)) {
            foreach($items as $item) {
                // Remove all items recursively
                unlink($file_path . "/" . $item);
            }
        }
    }
}
?>