<?php 
class Modal {
    
    //FUNCTION: Creates a modal specific to a type and puts it on the page
    //modal_title: The title that should be displayed on the modal
    //modal_fields: An associative array with all the fields in the modal.....
    //modal_planned_action: A string that dictates which action is going to be taken (Example: Delete, edit)
    //modal_button_name: form name of the submit button
    public function construct_modal($modal_title, $modal_fields, $modal_planned_action, $modal_button_name, $label_only = false){
            $str = '
            <div class="modal fade" id="' . $modal_title . '-modal-'.$modal_planned_action.'" tabindex="-1" role="dialog" aria-labelledby="' .$modal_title . '-modal"
           aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="modal-label">'.$modal_planned_action.' - ' . $modal_title . '</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <form id="modal-form" method="post" enctype="multipart/form-data">
                       <div class="modal-body">
                           '; // Construct the modal based on a foreach loop that loops in all the given fields
                           if($modal_fields != false) {
                           foreach($modal_fields as $field) {
                               $str .= ' <div class="form-group">
                               <label for="'.$field["title"].'" class="col-form-label">' . $field["title"] . '</label>
                               ';if($label_only == false){
                                    $str.='<input maxlength = "'.$field["max_length"].'" name="'.$field["name"].'" type="'.$field["type"].'" accept="'.$field["accepts"].'" multiple="'.$field["multiple"].'"  class="form-control" '.$field["multiple"].' >
                               ';}
                           $str.='</div>';
                           }
                        };
                           $str .= '

                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button name="'.$modal_button_name.'" type="submit" class="btn btn-primary submit-modal">'.$modal_planned_action.' ' . $modal_title . '</button>
                       </div>
                   </form>
               </div>
           </div>
       </div> 
           ';
           // Check if it isnt empty for error handling if not return it
           if(!empty($str)) {
           echo $str;
           } else {
            echo "<div class='col-sm-12''><p>Something went wrong please try again</p></div>";
           }

       }
    }
?>