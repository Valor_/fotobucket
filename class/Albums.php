<?php 
class  Albums extends Database {

    public function __construct() {
        $conn = Database::constructConnection();
    }

    // Adds an album to the database and its corresponding images to the file structure
    // $postValues: The $_POST 
    // $postFiles: The $_FILES
    public function add_album($postValues, $postFiles) {
        // Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($postValues);
        
        //  Check if the album name already exists
        $album_name_exists = $this->check_if_album_name_exists($sanVal['album-name']);
        // Check if $sanVal is valid and the album name doesnt exist already...
        if($sanVal && $album_name_exists == false) {
            // Get dateTime
            $dateAndTime = date('Y-m-d H:i:s');  
            
            // Save the original files
            $files = new Files();
            $album_path = $files->save_files($postFiles);
            
            // Save the thumbnails
            $photo_items = array_diff(scandir($album_path, 1), array('..', '.'));
            foreach($photo_items as $photo) { // Loop thru each photo
                // Get the image
                $image = $album_path . "/" . $photo;

                // Resize then save our new "thumbnail"
                $resizeObj = new Resize($image);
                $resizeObj -> resizeImage(300, 250, 'crop');
                $resizeObj -> saveImage($album_path . "/thumbnail_" . $photo , 100);
            }

            // Also insert the entry into the database
            $query = $this->dbh->prepare("INSERT INTO albums (`user_id`, `album_name`, `album_path`, `created`) 
            VALUES (:user_id, :album_name, :album_path, :created)");
        
            $query->bindParam(':user_id', $_SESSION['user_id'] );
            $query->bindParam(':album_name', $sanVal['album-name']);
            $query->bindParam(':album_path', $album_path);
            $query->bindParam(':created', $dateAndTime);
            $query->execute();

            // Refresh the page for instaload
            echo "<meta http-equiv='refresh' content='0'>";

        } else {
            $this->display_message("An album already exists with that name.. Or something went wrong");
        }
    }

    // Deletes an album from the database and its corresponding images and folder from the file structure
    // $postValues: The $_POST 
    public function delete_album($postValues) {
        // Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($postValues);
        if($sanVal) {

            // Get the album information
            $albums = $this->get_album_information($sanVal["album_id"]);

            // Create new Files instance and delete the corresponding images and folder
            $files = new Files;
            $files->delete_files_folder($albums[0]->album_path);

            // NOTE: We delete the share links first because album cant delete if they exist because of FKEY constraints
            // Delete potential shares to other users....
            $share_query = $this->dbh->prepare("DELETE FROM shared_albums WHERE album_id = ?");
            $share_query->execute(array($sanVal["album_id"]));

            // Deviating from standard delete function in Database class because of special case.... 
            // Userid and id have to be checked before deletion
            $query = $this->dbh->prepare("DELETE FROM albums WHERE id = ? AND user_id = ?");
            $query->execute(array($sanVal["album_id"],  $_SESSION["user_id"]));

            // Refresh the page for instaload
            echo "<meta http-equiv='refresh' content='0'>";
        } 
    }

    // Shares an album to a specific user email adress
    // $postValues: The $_POST
    public function share_album($postValues) {
        // Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($postValues);

        if($sanVal) {
            // Get dateTime
            $dateAndTime = date('Y-m-d H:i:s');  

            // Get the user_id of the recipent based on the e-mail adress
            $recipient_query = $this->dbh->prepare("SELECT id FROM users WHERE email = ? LIMIT 1");
            $recipient_query->execute(array(strtolower($sanVal["album_email"])));
            $recipient = $recipient_query->fetch();

            // Note the user_id here belongs to the reciever of the album....
            $query = $this->dbh->prepare("INSERT INTO shared_albums (`shared_on`, `album_id`, `user_id`) 
            VALUES (:shared_on, :album_id, :recipient_id)");

            // Execute the query and create the shared album "link"
            $query->bindParam(':shared_on', $dateAndTime );
            $query->bindParam(':album_id', $sanVal['album_id']);
            $query->bindParam(':recipient_id', $recipient["id"]);
            $query->execute();
        
        }
    }

    // Load all the albums
    public function load_albums() {
        // Fetch all the albums based on the current user
        $query = $this->dbh->prepare("SELECT * FROM albums WHERE user_id = ?");
        $query->execute(array($_SESSION["user_id"]));
        $albums = $query->fetchAll(PDO::FETCH_CLASS);
        
        // Check if there are any shared albums
        $shared_albums = $this->get_shared_albums();

        // If there are add them to the $albums object
        if($shared_albums) {
            $the_albums = [];

            // Get information for each shared album
            foreach($shared_albums as $item) {
                $album_query = $this->dbh->prepare("SELECT * FROM albums WHERE id = ? LIMIT 1");
                $album_query->execute(array($item->album_id));
                $single_shared_album = $album_query->fetchAll(PDO::FETCH_CLASS);

                // Add it to the albums object
                array_push($albums, $single_shared_album[0]);
            }
        }

        // Check if albums is valid
        if($albums) {
        foreach($albums as $album) {
            // Set variables
            $album_name = $album->album_name;
            $album_date = new DateTime($album->created);
            $album_date = date_format($album_date, "Y/m/d");
            $album_path = $album->album_path;
            $photo_items = array_diff(scandir($album_path, 1), array('..', '.'));

            // Default values
            $shared = null;
            $shared_get = null;
            $image_one = "assets/images/kitten.jpg";
            $image_two = "assets/images/kitten.jpg";

            // Check how many photos we need to display on the preview and if not use stock photo
            if(count($photo_items) >= 1) {
                $image_one =  $album_path . "/" . $photo_items[0];
                $image_two =  $album_path . "/" . $photo_items[1];
            } else if(count($photo_items) > 0) { // Check photo count
                $image_one =  "user-files/" . $_SESSION["user_id"] . "/" . $album_name . "/" . $photo_items[0];
                $image_two = "assets/images/kitten.jpg";
            } 

            // Check if the current album is a shared album
            if($_SESSION["user_id"] != $album->user_id) {
                // Set values
                $shared = " - Shared Album";
                $shared_get = "&shared=$album->user_id";
            }

            // Load the album
            $str = '
            <div class="col-12 col-md-4">
                <div data-id="'.$album->id.'"class="album-wrapper">
                <a class="album-link" href="album-view?album='.$album->id.$shared_get.'">
                    <div class="album-image-wrapper">
                        <img class="img-fluid" src="'.$image_one.'" alt="Album image">
                        <img class="img-fluid" src="'.$image_two.'" alt="Album image">
                    </div>
                    </a>
                    <div class="album-content-wrapper">
                        <div class="top-row">
                            <p class="name-album">'.$album_name.$shared.'</p>

                            '; if($shared == null) { $str.='
                            <div class="icon-wrap">
                                <i class="icon-pencil edit-album"></i>
                                    <i class="icon-cancel delete-album"></i>
                            </div>
                            ';} $str .='
                        </div>
                        <div class="date-wrap">
                            <p class="date-album">Date: '.$album_date.'</p>
                        </div>
                    </div>
                </div>
            </div>
            ';
            // Display it
            echo $str;
        }
      } else {
        // Error handling....
        $this->display_message("No albums to show, add some!");
        }
    }

    // Update the album name
    // $postValues; The $_POST
    // $album_id : Not standardly given only in specific cases, speaks for itself
    public function update_album_name($postValues, $album_id = false) {
        $sanVal = $this->validateSanatize($postValues);

        // Check if album_id should be gotten from $sanVal or from an already given id
        if($album_id == false) {
            $album_id = $sanVal['album_id'];
        }
        if($sanVal) {
            // Create update query
            $query = $this->dbh->prepare("UPDATE albums SET album_name=:album_name WHERE id = :id AND user_id = :user_id");
            $query->bindParam(':album_name', $sanVal['album_edit_name'] );
            $query->bindParam(':id', $album_id);
            $query->bindParam(':user_id', $_SESSION['user_id']);

            // Execute it
            $query->execute();

            // Refresh the page for instaload
            echo "<meta http-equiv='refresh' content='0'>"; 
        }
    }

    // Loads album images
    // $album: Object, contains all the information about the current album
    public function load_album_images($album) {

        // Check if popup is to be displayed because of reload
        if(isset($_SESSION["display_message"])) {
            $this->display_popup($_SESSION["display_message"], "green");
            unset($_SESSION["display_message"]);
        }

        // Get both the thumbnails and full size images
        $photo_thumbnails = $this->get_album_thumbnails($album);
        $photos_fullsize = $this->get_album_images($album);

        // Check if theyre not empty and then loop thru all photos
        $index = 0;
        if(!empty($photos_fullsize) && !empty($photo_thumbnails)) {
            foreach($photo_thumbnails as $photo_thumbnail) {
                // Set the paths for both
                $path_thumbnail = $album->album_path . "/" . $photo_thumbnail;
                $path_fullsize =   $album->album_path . "/" . $photos_fullsize[$index];

                // Display them!
                echo'
                <div class="image_wrap">
                    <span data-image="'.$photos_fullsize[$index].'" class="select-item"></span>
                    <a class="link-image" data-fancybox="gallery" href="'.$path_fullsize.'">
                        <img class="img-fluid" src="'.$path_thumbnail.'">
                    </a>
                </div>
                ';
                $index++;
            }
            return true;
        } else {
            // Error handling
            $this->display_message("Your album appears to be empty :O");
            return false;
        }
    }

    // Uploads photos to the file structure
    // $postFiles: The $_FILES
    // $album_path: The path to the album in question
    public function upload_photos($postFiles, $album_path) {
          // Save the original files
          $files = new Files();
          $fileArray = $files->save_files($postFiles, $album_path);

          // Save the thumbnails
          for($i = 0; $i < count($fileArray); $i++) {
              // Loop through each file and save them as thumbnails
              $image = $album_path . "/" . $fileArray[$i];

              // Resize and save the image  to the path
              $resizeObj = new Resize($image);
              $resizeObj -> resizeImage(300, 250, 'crop');
              $resizeObj -> saveImage($album_path . "/thumbnail_" . $fileArray[$i] , 100);
          }
          // Reset userfiles to ensure files doesnt create a double upload...
          $_FILES["userfiles"] = null;
          // Set popup for refresh
          $_SESSION["display_message"] = "Added photos";

          // Refresh the page for instaload
          echo "<meta http-equiv='refresh' content='0'>"; 
    }

    // Deletes specifically selected photos
    // $postValues: The $_POST
    // $album_path: The specific album path
    public function delete_photos($postValues, $album_path) {
         // Validate and sanatize the postvalues
        $sanVal = $this->validateSanatize($postValues);
        $items = [];

        // For each photo
        foreach($sanVal as $value) {
            if(!empty($value)) {
                // We push it twice because we have both a thumbnail and a regular image
                array_push($items, $value);
                array_push($items, "thumbnail_". $value);
            }
        }
        // Delete the files... create a new files object and delete
        $files = new Files;
        $files->delete_files($items, $album_path);

        // Set popup message
        $_SESSION["display_message"] = "Deleted photos";

        // Refresh the page for instaload
        echo "<meta http-equiv='refresh' content='0'>"; 
    }

    // Checks if the album name already exists in the database.
    //$album_name: The name to check
    private function check_if_album_name_exists($album_name) {
        $query = $this->dbh->prepare("SELECT album_name FROM albums WHERE album_name = ?");
        $query->execute(array($album_name));
        $album_name = $query->fetchAll(PDO::FETCH_CLASS); 

        // Return true or false based on the query
        if($album_name) {
            return true;
        } else { 
            return false;
        }
    }

    // Get all shared albums of the current user
    private function get_shared_albums() {
        $query = $this->dbh->prepare("SELECT * FROM shared_albums WHERE user_id = ?");
        $query->execute(array($_SESSION["user_id"]));
        $items = $query->fetchAll(PDO::FETCH_CLASS); 
        // Check if the user has any shared albums if so return them
        if($items) {
                return $items;
            }
    }

    // Get the album information of a specific album
    // $album_id: The id of the album to get
    // $user_id: Standardly based on the id of the current user except in shared albums
    private function get_album_information($album_id, $user_id = null) {
        if($user_id == null) {
            $user_id == $_SESSION["user_id"];
        }
        // Get the albums...
        $query = $this->dbh->prepare("SELECT * FROM albums WHERE id = ? AND user_id = ?");
        $query->execute(array($album_id,  $_SESSION["user_id"]));;
        $albums = $query->fetchAll(PDO::FETCH_CLASS); 

        // Check if there are any
        if($albums) {
            // Return them
            return $albums;
        }
    }

    // Get all album images  of a specific album
    // $album: Object with all info of the specific album
    private function get_album_images($album) {
        $items = array_diff(scandir($album->album_path, 1), array('..', '.'));
        $items = preg_grep("/thumbnail_/", $items, PREG_GREP_INVERT);
        $items = array_values($items);
        return $items;
    }

    // Get all album thumbnails of a specific album
    // $album: Object with all info of the specific album
    private function get_album_thumbnails($album) {
        $items = array_diff(scandir($album->album_path, 1), array('..', '.'));
        $items =  preg_grep ("/^thumbnail_.*/", $items);
        return $items;
    }

    // Validates and sanatizes given values
    // $values: The given values, works with keys so i.e. "test"=>"hello"
    private function validateSanatize($values) { 
        $data_array = [];

        // Loop thru them all
        foreach($values as $key => $value){
            $value = addslashes($value);
            $value = preg_replace("/<script>|<\/script>/i", "", $value);
            $value = preg_replace("/<|>/i", "", $value);
            $value = strip_tags($value);
            $data_array[$key] = $value;
        }
        // Return them after cleaning
        return $data_array;
    }

        // Displays a message
        // $message: The message
        public function display_message($message) { 
            echo '<div class="col-12 mt-4"><h4>' . $message . '</h4></div>';
        }
    
        // Display a popup with given text and color
        // $message: The message
        // $color: The color
        public function display_popup($message, $color) {
            echo '<div style="background-color:'.$color.';" class="popup"><p>'.$message.'</p></div>';
        }
}
?>