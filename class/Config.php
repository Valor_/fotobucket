<?php
class Config{
    public $project_base;

    public function __construct(){
        $this->project_base  = dirname(dirname(__FILE__)) . '/';
    }

    public $project_name = "fotobucket";
    public $project_root = "login";
    
    //Enviroment can be:
    // localhost:
    // quinnesselbrugge.icthardenberg.nl
    // quinnesselbrugge.nl

    // public $project_environment = "http://quinnesselbrugge.icthardenberg.nl";
    
}

?>