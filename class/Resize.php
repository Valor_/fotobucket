<?php 
class Resize {
    private $image;
    private $width;
    private $height;
    private $imageResized;

    public function __construct($fileName) {
     // Open image
      $this->image = $this->openImage($fileName);
     
      // Set width and height
      $this->width  = imagesx($this->image);
      $this->height = imagesy($this->image);
    }

    // Open image
    // $file: The file
    public function openImage($file) {
    // Get the extension
    $extension = strtolower(strrchr($file, '.'));
 
    switch($extension)
    {
        case '.jpg':
        case '.jpeg':
            $img = @imagecreatefromjpeg($file);
            break;
        case '.gif':
            $img = @imagecreatefromgif($file);
            break;
        case '.png':
            $img = @imagecreatefrompng($file);
            break;
        default:
            $img = false;
            break;
    }
    return $img;
}

// Save the image
// $savePath: The path to save to
// $imageQuality: quality of the image
public function saveImage($savePath, $imageQuality="100") {
    //  Get extension
    $extension = strrchr($savePath, '.');
    $extension = strtolower($extension);
 
    switch($extension)
    {
        case '.jpg':
        case '.jpeg':
            if (imagetypes() & IMG_JPG) {
                imagejpeg($this->imageResized, $savePath, $imageQuality);
            }
            break;
 
        case '.gif':
            if (imagetypes() & IMG_GIF) {
                imagegif($this->imageResized, $savePath);
            }
            break;
 
        case '.png':
            //  Scale quality from 0-100 to 0-9
            $scaleQuality = round(($imageQuality/100) * 9);
 
            //  Invert quality setting as 0 is best, not 9
            $invertScaleQuality = 9 - $scaleQuality;
 
            if (imagetypes() & IMG_PNG) {
                imagepng($this->imageResized, $savePath, $invertScaleQuality);
            }
            break;
 
        // ... etc
 
        default:
            // No extension - No save.
            break;
    }
 
    imagedestroy($this->imageResized);
}

// Resize the image
// $newWidth: The new width of the image
// $newHeight: The new height of the image
// $option: What to do with the image
public function resizeImage($newWidth, $newHeight, $option="auto") {
 
    //  Get optimal width and height - based on $option
    $optionArray = $this->getDimensions($newWidth, $newHeight, strtolower($option));
 
    $optimalWidth  = $optionArray['optimalWidth'];
    $optimalHeight = $optionArray['optimalHeight'];
 
    //  Resample - create image canvas of x, y size
    $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
    imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);

    if ($option == 'crop') {
        $this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
    }
}

// Get the dimensions
// $newWidth: The new width
// $newHeight: The new height
// $option: The selected option
private function getDimensions($newWidth, $newHeight, $option) {
 
   switch ($option)
    {
        // Look through all options and act according to one
        case 'exact':
            $optimalWidth = $newWidth;
            $optimalHeight= $newHeight;
            break;
        case 'portrait':
            // Get size and set optimal height
            $optimalWidth = $this->getSizeByFixedHeight($newHeight);
            $optimalHeight= $newHeight;
            break;
        case 'landscape':
            // Set optimalwidth and get the size via fixed width
            $optimalWidth = $newWidth;
            $optimalHeight= $this->getSizeByFixedWidth($newWidth);
            break;
        case 'auto':
            // Get the width and height automatically
            $optionArray = $this->getSizeByAuto($newWidth, $newHeight);
            $optimalWidth = $optionArray['optimalWidth'];
            $optimalHeight = $optionArray['optimalHeight'];
            break;
        case 'crop':
            // Get the optimal crop and set width and height
            $optionArray = $this->getOptimalCrop($newWidth, $newHeight);
            $optimalWidth = $optionArray['optimalWidth'];
            $optimalHeight = $optionArray['optimalHeight'];
            break;
        }
        // Return the values
        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    // Crop the image potentially
    // $optimalWidth: The optimal width for the crop
    // $optimalHeight: The optimal height for the crop
    // $newWidth: The new width for the crop
    // $newHeight: The new height for the crop
    private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight) {
    // Find center - this will be used for the crop
    $cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
    $cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );
    
    $crop = $this->imageResized;
    
    // Now crop from center to exact requested size
    $this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
    
    // Retain transparency for PNG images
    if (imagetypes() & IMG_PNG) {
        imagealphablending($this->imageResized, false);
        imagesavealpha($this->imageResized, true);
        $bg = imagecolorallocatealpha($this->imageResized, 255, 255, 255, 127);
        imagefilledrectangle($this->imageResized, 0, 0, $optimalWidth, $optimalHeight, $bg);
    }

    // Copy the image based on the crop
    imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);
    }

    // Get the optimal crop zone
    // $newWidth: New width of the image
    // $newHeight: New height of the image
    private function getOptimalCrop($newWidth, $newHeight) {
    // Set height and width ratio
    $heightRatio = $this->height / $newHeight;
    $widthRatio  = $this->width /  $newWidth;
 
    /// Check if there is no collusion
    if ($heightRatio < $widthRatio) {
        $optimalRatio = $heightRatio;
    } else {
        $optimalRatio = $widthRatio;
    }
 
    // Set the optimal height and width
    $optimalHeight = $this->height / $optimalRatio;
    $optimalWidth  = $this->width  / $optimalRatio;
 
    // Return it
    return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
}

// Get size fixed height
// $newHeight: The new height of the image
private function getSizeByFixedHeight($newHeight) {
    // Determine ratio
    $ratio = $this->width / $this->height;
    $newWidth = $newHeight * $ratio;
    return $newWidth;
}
 
// Get size fixed width
// $newWidth: The new width of the image
private function getSizeByFixedWidth($newWidth) {
    // Determine ratio
    $ratio = $this->height / $this->width;
    $newHeight = $newWidth * $ratio;
    return $newHeight;
}
 
// Get size automatically
// $newWidth: The new width of the image
// $newHeight: The new height of the image
private function getSizeByAuto($newWidth, $newHeight) {
    if ($this->height < $this->width) {
         //  Image to be resized is wider (landscape)
        $optimalWidth = $newWidth;
        $optimalHeight= $this->getSizeByFixedWidth($newWidth);
    }
    elseif ($this->height > $this->width)     {
        //  Image to be resized is taller (portrait)
        $optimalWidth = $this->getSizeByFixedHeight($newHeight);
        $optimalHeight= $newHeight;
    }
    else {  //  Image to be resized is a square
        if ($newHeight < $newWidth) {
            $optimalWidth = $newWidth;
            $optimalHeight= $this->getSizeByFixedWidth($newWidth);
        } else if ($newHeight > $newWidth) {
            $optimalWidth = $this->getSizeByFixedHeight($newHeight);
            $optimalHeight= $newHeight;
        } else {
            //  Square being resized to a square
            $optimalWidth = $newWidth;
            $optimalHeight= $newHeight;
        }
    }
    // Return it
    return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
}
}
?>