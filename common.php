<?php 
class autoLoader {
    public static function myAutoloader($className) {

            $path = __DIR__  . "/class/";
            include $path.$className.'.php'; //Only include in the whole project because you never leave your classes again.
        }
  }
  //Call "autoLoader" class and the function that belongs to it to autoload the classes.
  //spl_autoload_register takes care of the hard stuff. :P
  //It basically registers all given functions from the loaded classes as an __autoload() implementation.
  spl_autoload_register(array('autoLoader', 'myAutoloader'));
?>