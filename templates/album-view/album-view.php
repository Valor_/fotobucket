<?php
    // Check if $_GET vvalue is set and is numeric
    if(isset($_GET["album"]) && is_numeric($_GET["album"])) {

        // Set variables
        $album_id = $_GET["album"];
        $user_id = $_SESSION["user_id"];
        $album_object = new Albums();
        $dbhelper = new Database();

        // Check if $_GET["Shared"] is set if it is take that value and check if any albums are shared under that id
        if(isset($_GET["shared"]) && is_numeric($_GET["shared"])) {
            // Prepare execute and fetch the result
            $check_shared = $dbhelper->dbh->prepare("SELECT * FROM shared_albums WHERE album_id = ? LIMIT 1");
            $check_shared->execute(array($album_id));
            $item = $check_shared->fetchAll(PDO::FETCH_CLASS);

            // If there is a result set the user_id to that of the original owner of the album to get the album info later
            if($item) {
                $user_id = $_GET["shared"];
            }
        }

        // Get the album info
        $query = $dbhelper->dbh->prepare("SELECT * FROM albums WHERE id = ? AND user_id = ?");
        $query->execute(array($album_id,  $user_id));
        $album = $query->fetchAll(PDO::FETCH_CLASS); 

        // If found
        if($album) {
        $album = $album[0];
        ?>
<section id="view-album">
    <div class="album-bar">
        <h1><?php echo $album->album_name?></h1>
        <?php if(!isset($_GET["shared"])){?>
        <i class="icon-pencil edit-album"></i>
        <?php }?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="image-wrapper">
                    <?php 
                        // Load all the album images
                       $load_images = $album_object->load_album_images($album);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="album-bottom-bar">
        <div class="album-actions-info">
        <?php if(!isset($_GET["shared"])){?>
            <i class="add-photos"></i>
            <i class="delete-photos"></i>
            <i class="icon-export share-photos"></i>
            <?php }?>
            <i class="icon-download download-photos"></i>
            <p class="date-album">Date: <?php $date = new DateTime($album->created); echo date_format($date, "Y/m/d") // Get and set the date?>
            </p>
        </div>
        <div class="album-download-all">
            <span class="download-all-images">
                <p>Download album</p>
            </span>
        </div>
    </div>
</section>

<?php 

        // Dont load if $_GET[shared] is set
        if(!isset($_GET["shared"])) {
            $modal = new Modal();

            // Construct all the modals needed for this page

            $fields_add[0] = ["title" => "Fotos", "name" => "userfiles[]", "type" => "file", "max_length" => "", "accepts" => "png,.jpg,.jpeg", "multiple" => "multiple"];
            $modal->construct_modal("Photos", $fields_add, "Add", "add_photos");

            $fields_delete[0] = ["title" => "Are you sure?"];
            $modal->construct_modal("Photos", $fields_delete, "Delete", "delete_photos", true);


            $fields_edit[0] = ["title" => "Album name", "name" => "album_edit_name", "type" => "text", "max_length" => "20",  "accepts" => "*", "multiple" => ""];
            $modal->construct_modal("Album", $fields_edit, "Edit", "edit_album");

            $fields_share[0] = ["title" => "User email", "name" => "album_email", "type" => "text", "max_length" => "50",  "accepts" => "*", "multiple" => ""];
            $modal->construct_modal("Album", $fields_share, "Share", "share_album");


            // Check all the $_POST endpoints and act accordingly

            if(isset($_POST["add_photos"])) {
                if(isset($_FILES["userfiles"])) {
                    $_POST["album_id"] = $album_id;
                    // Upload the photos
                    $album_object->upload_photos($_FILES, $album->album_path);
                }
            }

            if(isset($_POST["delete_photos"])) {
                // Delete the photos
                $album_object->delete_photos($_POST, $album->album_path);
            }

            if(isset($_POST["edit_album"])) {
                    $_POST["album_id"] = $album_id;
                    // Update the album name
                    $album_object->update_album_name($_POST, $album_id);
            }

            if(isset($_POST["share_album"])) {
                if (filter_var($_POST["album_email"], FILTER_VALIDATE_EMAIL)) {
                    $_POST["album_id"] = $album_id;
                    // Share the album
                    $album_object->share_album($_POST);
                } else {
                    // Error handling
                    $album_object->display_popup("Invalid email!", "red");
                }
            } 
        }
            } else {
                echo "<h2 style='text-align: center;'>I think you got lost.... <a href='albums'>Return home?</a></h2>";
            }
        } else {
            echo "<h2 style='text-align: center;'>I think you got lost.... <a href='albums'>Return home?</a></h2>";
        }
        ?>