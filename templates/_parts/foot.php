<script>
    var getUrl = window.location;
    const BASE_DIR = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
</script>
<script src="assets/scripts/jquery.js"></script>
<script src="assets/scripts/jquery.fancybox.min.js"></script>
<script src="assets/scripts/jszip-utils.min.js"></script>
<script src="assets/scripts/jszip.min.js"></script>
<script src="assets/scripts/FileSaver.js"></script>
<script src="externals/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/scripts/main.js"></script>
</body>
</html>