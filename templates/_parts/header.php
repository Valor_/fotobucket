<section id="header">
    <div class="header-wrapper">
        <div class="row">
            <div class="col-12 col-sm-6">
                <a href="albums">
                    <img alt="Logo" class="img-fluid logo" src="assets/images/logo.svg" alt="">
                </a>
            </div>
            <div class="col-12 col-sm-6">
                <div class="menu-wrapper">
                <ul class="menu-items">
                        <li>
                        <a href="logout">Logout</a>
                        </li>   
                    </ul>
                    <p class="menu">Menu</p>
                </div>
            </div>
        </div>
    </div>
</section>