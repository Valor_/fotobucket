<section id="albums">
    <div class="container">
        <div class="row">
            <?php 
                // Load all the albums
                $album_object = new Albums();
                $album_object->load_albums();
            ?>
        </div>
        <div class="add-items"></div>
        <?php 

        // Set all the modals needed on this page
        $modal = new Modal;

        $fields_add[0] = ["title" => "Album name", "name" => "album-name", "type" => "text", "max_length" => "20",  "accepts" => "*", "multiple" => ""];
        $fields_add[1] = ["title" => "Fotos", "name" => "userfiles[]", "type" => "file", "max_length" => "", "accepts" => "png,.jpg,.jpeg", "multiple" => "multiple"];
        $modal->construct_modal("Album", $fields_add, "Add", "add_album");

        $fields_edit[0] = ["title" => "Album name", "name" => "album_edit_name", "type" => "text", "max_length" => "20",  "accepts" => "*", "multiple" => ""];
        $modal->construct_modal("Album", $fields_edit, "Edit", "edit_album");


        $fields_delete[0] = ["title" => "Are you sure?"];
        $modal->construct_modal("Album", $fields_delete, "Delete", "delete_album", true);

        // All the $_POST endpoints
        if(isset($_POST['add_album'])){ 
            // Add the album
            $album_object->add_album($_POST, $_FILES);
        }
        
        if(isset($_POST["delete_album"]) && isset($_POST["album_id"]) && is_numeric($_POST["album_id"])) {
            // Delete the album
            $album_object->delete_album($_POST);
        }

        if(isset($_POST["edit_album"])) {
            // Update album name
            $album_object->update_album_name($_POST);
        }
        ?>
    </div>
</section>