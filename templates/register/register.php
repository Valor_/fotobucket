<section id="register">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="form-wrapper">
                <form method="POST" class="form-horizontal">
                    <h1 class="col-sm-12">Register</h1>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="username">Username:</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email" placeholder="Enter username"
                                name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" placeholder="Enter email"
                                name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Password:</label>
                        <div class="col-sm-12">
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="pwd">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button name="submit" type="submit" class="btn btn-default">Register</button>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <p>Need to login? <a href="login"> Login</a></p>
                    </div>
                    <?php 
                        if(isset($_POST["submit"])){
                            // Register the user
                            $register = new Register;
                            $register->registerUser();
                        }
                        ?>
                </form>
            </div>
        </div>
    </div>
</section>