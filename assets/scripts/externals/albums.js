// Array of currently selected  images
let selected_images = [];
let select_item = $(".image-wrapper").find(".select-item");

// Displays the modal and adds potential extra fields
function display_modal(title, action, hidden_values = false, key, values) {
    // Show the modal
    let modal = $('body').find("#" + title + '-modal-' + action);
    $(modal).modal('show');

    if (hidden_values == true) {
        // Find form and append our values
        let form = $(modal).find("#modal-form");

        if (values.length > 1) {
            for (let i = 0; i < values.length; i++) {
                // Check if the values dont already exist in the form
                if (!$(form).find("." + key + i).length) {
                    // Append them
                    let str = '<input class="' + key + i + '" type="hidden" name="' + key + i + '"  value="' + values[i] + '">';
                    $(form).append(str);
                }
            }
        } else {
            if (!$(form).find("." + key).length) {
                // Append them
                let str = '<input class="' + key + '" type="hidden" name="' + key + '"  value="' + values[0] + '">';
                $(form).append(str);
            }
        }
    }
}

// Downloads a single image, pretty straight forward
function download_image(href) {
    var link = document.createElement('a');
    link.href = href;
    link.download = 'Download.jpg';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

var Promise = window.Promise;
if (!Promise) {
    Promise = JSZip.external.Promise;
}

// Fetch the content and return the associated promise.
// String url the url of the content to fetch.
// Promise the promise containing the data.
function urlToPromise(url) {
    return new Promise(function (resolve, reject) {
        JSZipUtils.getBinaryContent(url, function (err, data) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// Downloads all selected images
function download_images(images) {
    // New JSzip instance
    var zip = new JSZip();
    // Loop thru all selected and add them to the zip
    for (let x = 0; x < images.length; x++) {
        var filename = images[x].replace(/.*\//g, "");
        zip.file(filename, urlToPromise(images[x]), {
            binary: true
        });
    }

    // Generate the zip and save it using filesaver.js
    zip.generateAsync({
        type: "blob"
    }).then(function (blob) { // 1) generate the zip file
        saveAs(blob, "photos.zip"); // 2) trigger the download
    }, function (err) {
        console.log(err);
    });
}

// Add item to currently selected images
function add_item(e) {
    if ($(e).hasClass("active-select-item")) {
        // Basic styling
        $(e).removeClass("active-select-item");
        let index = selected_images.indexOf($(e).data("image"));
        if (index !== -1) {
            // Remove path from array
            selected_images.splice(index, 1);
        }
    } else {
        // Basic styling
        $(e).addClass("active-select-item");

        // Add it to the array
        selected_images.push($(e).data("image"));
    }
}

// Opens and controls the menu
function open_menu(settings_ul) {
    e = $(settings_ul);

    // Check if element has class active if it doesnt add it if it does remove it and animate it
    if (e.hasClass('active')) {
        $(e).find('li').each(function (li) {
            $(this).css('transform', 'translateY(50px)');
        });
        e.removeClass('active');
    } else {
        $(e).find('li').each(function () {
            $(this).css('transform', 'translateY(0px)');
        });
        e.addClass('active');
    }
}