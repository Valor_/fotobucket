$(document).ready(function () {

    // Load all scripts
    get_scripts();

    //Stops forms from resubmitting
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }

    // Stop form from resubmitting on double click
    $("body").find("#modal-form").submit(function (event) {
        $(this).find(".submit-modal").click(function (e) {
            event.preventDefault();
            $(this).prop('disabled', true);
        });
    });
});

function get_scripts() {
    $.getScript("assets/scripts/externals/albums.js", function () {

        // Menu controls
        $(".menu").click(function () {
            var ul = $(".menu-items");
            open_menu(ul);
        });

        // Displays add item modal
        $("section#albums").find(".add-items").click(function () {
            display_modal("Album", "Add");
        });

        // Displays delete album modal
        $("section#albums").find(".delete-album").click(function () {
            var album_id = $(this).closest(".album-wrapper").data("id");
            display_modal("Album", "Delete", true, "album_id", [album_id]);
        });
        // Displays edit album modal
        $("section#albums").find(".edit-album").click(function () {
            var album_id = $(this).closest(".album-wrapper").data("id");
            display_modal("Album", "Edit", true, "album_id", [album_id]);
        });

        // Displays edit album modal fot inspecting albums
        $("section#view-album").find(".edit-album").click(function () {
            display_modal("Album", "Edit");
        });

        // Displays add photos modal
        $("section#view-album").find(".add-photos").click(function () {
            display_modal("Photos", "Add");
        });

        // Displays delete photos modal
        $("section#view-album").find(".delete-photos").click(function () {
            var selected = $(".image-wrapper").find(".active-select-item");
            if (selected.length) {
                display_modal("Photos", "Delete", true, "photo-paths", selected_images);
            }
        });

        // Handles the downloading of zip files and single files
        $("section#view-album").find(".download-photos").click(function () {
            // Get selected images
            var selected = $(".image-wrapper").find(".active-select-item");
            let parent = $(selected).parent();

            // If it has 2 or more images
            if (selected.length >= 2) {
                let image_array = [];
                // Loop through all selected and get the link href
                for (let x = 0; x < selected.length; x++) {
                    let image_link = $(parent[x]).find(".link-image").attr("href");

                    // Push to array
                    image_array.push(image_link);
                }

                // Zip and download the images based on that array
                download_images(image_array);
            } else if (selected.length) { // If it doesnt have 2 or more download a single image
                var href = $(parent).find(".link-image").attr("href");
                download_image(href);
            }
        });

        // Downloads the whole album into a zip
        $("section#view-album").find(".download-all-images").click(function () {
            let image_array = [];
            let image_links = $(".image-wrapper").find(".link-image");
            // Loop through al image links in the album
            for (let x = 0; x < image_links.length; x++) {
                // Get the links and put them in an array
                image_array.push($(image_links[x]).attr("href"));
            }
            // Download the zip based on that array
            download_images(image_array);
        });

        // Add an item to the selected_items array
        $("section#view-album").find(".select-item").click(function () {
            add_item(this);
        });

        // Open the share album modal
        $("section#view-album").find(".share-photos").click(function () {
            display_modal("Album", "Share");
        });
    });
}