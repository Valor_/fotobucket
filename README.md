Opzetten CodeMuffin

1) Importeer de meegegeven database in het _SQL mapje naar je SQL manager naar keuze
        Let op: Zorg dat de naam goed overeenkomt met de volgende stap
2) Pas in class/Database.php de CONST waardes in zodat deze jouw database inrichting matchen
3) Pas eventueel class/Config.php aan (Waarschijnlijk niet) Note: $project_enviroment alleen aanzetten als hij live staat
4) Check of je op je lokale omgeving naar b.v.b. 127.0.0.1/fotobucket kan gaan....

Gaat dit goed en zie je de login pagina? Top, je bent klaar!

Zo niet? Neem contact op met de hoofdeveloper voor on-site support: q.esselbr@gmail.com

Notitie: Als je SCSS wijzigingen moet doen moet je een SASS compiler gebruiken...

Veel plezier met nakijken :)